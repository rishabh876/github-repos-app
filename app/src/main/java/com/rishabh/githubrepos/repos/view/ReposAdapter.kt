package com.rishabh.githubrepos.repos.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.rishabh.githubrepos.R
import com.rishabh.githubrepos.models.repository.Repository

class ReposAdapter(private val reposList: ArrayList<Repository>, private val listener: Listener) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ITEMS = 1
    private val FOOTER = 2
    private var isFooterVisible = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEMS -> {
                LayoutInflater.from(parent.context).inflate(R.layout.repo_list_item_layout, parent, false)
                        .let { ViewHolder(it) }
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.footer_progress, parent, false)
                ProgressFooterViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        var itemCount = reposList.size
        if (isFooterVisible)
            itemCount++
        return itemCount
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.bind(reposList[position])
        } else if (holder is ProgressFooterViewHolder) {

        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (reposList.size > position) {
            ITEMS
        } else {
            FOOTER
        }
    }

    fun addItems(reposList: List<Repository>) {
        val position = this.reposList.size
        this.reposList.addAll(reposList)
        notifyItemRangeInserted(position, reposList.size)
    }

    fun showFooter() {
        if (!isFooterVisible) {
            isFooterVisible = true
            notifyItemInserted(itemCount - 1)
        }
    }

    fun removeFooter() {
        if (isFooterVisible) {
            isFooterVisible = false
            notifyItemRemoved(itemCount)
        }
    }

    interface Listener {
        fun onRepoClicked(repository: Repository)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.repo_container)
        lateinit var repoContainerView: View
        @BindView(R.id.repo_name_tv)
        lateinit var repoNameTv: TextView
        @BindView(R.id.repo_desc_tv)
        lateinit var repoDescTv: TextView
        @BindView(R.id.lang_tv)
        lateinit var langTv: TextView
        @BindView(R.id.star_tv)
        lateinit var starTv: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

        fun bind(repository: Repository) {
            repoNameTv.text = repository.name
            if (repository.description != null) {
                repoDescTv.text = repository.description
                repoDescTv.visibility = View.VISIBLE
            } else {
                repoDescTv.visibility = View.GONE
            }

            langTv.text = "● ${repository.language ?: "NA"}"
            starTv.text = "★ ${repository.stargazersCount ?: 0}"
            repoContainerView.setOnClickListener { listener.onRepoClicked(repository) }
        }
    }

    inner class ProgressFooterViewHolder(view: View) : RecyclerView.ViewHolder(view)
}