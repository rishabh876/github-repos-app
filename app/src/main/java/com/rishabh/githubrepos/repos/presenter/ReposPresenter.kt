package com.rishabh.githubrepos.repos.presenter

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.rishabh.githubrepos.models.repository.Repository
import com.rishabh.githubrepos.network.GithubService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject

class ReposPresenter @Inject constructor(private val apiService: GithubService) : MvpBasePresenter<ReposPresenter.View>() {

    private lateinit var username: String
    private var nextPage = 1
    private var isReachedEnd = false
    private var isShowingContent = false
    private var isLoading = false
    private var disposable: Disposable? = null

    fun onUserNameSubmit(query: String?) {
        if (query == null) {
            return
        }
        isReachedEnd = false
        nextPage = 1
        isShowingContent = false
        username = query
        disposable?.dispose()
        ifViewAttached({
            it.showFullscreenProgress()
            it.hideEmptyState()
            it.clearList()
        })
        getRepoList(query)
    }

    private fun getRepoList(username: String) {
        apiService.getUserReposList(username, page = nextPage++)
                .doOnSubscribe { isLoading = true }
                .doFinally { isLoading = false }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onGetRepoListSuccess(it) }
                        , { onGetRepoListFailed(it) })
                .apply { disposable = this }
    }

    private fun onGetRepoListSuccess(response: Response<List<Repository>>?) {
        ifViewAttached({
            it.hideFullscreenProgress()
            it.hideFooterLoader()
            if (response?.body() == null || response.body()?.isEmpty() == true) {
                if (isShowingContent) {
                    isReachedEnd = true
                } else {
                    it.showEmptyState()
                }
            } else {
                it.addRepos(response.body()!!)
                isShowingContent = true
            }
        })
    }

    private fun onGetRepoListFailed(error: Throwable) {
        nextPage--
        ifViewAttached({
            it.hideFooterLoader()
            it.hideFullscreenProgress()
            it.showError(error.localizedMessage)
        })
    }

    fun onRepositoryClicked(repository: Repository) {
        ifViewAttached { it.openPullRequestScreen(repository, username) }
    }

    fun getMoreRepos() {
        if (!isReachedEnd && !isLoading) {
            ifViewAttached { it.showFooterLoader() }
            getRepoList(username)
        }
    }

    override fun destroy() {
        super.destroy()
        disposable?.dispose()
    }

    interface View : MvpView {
        fun addRepos(repos: List<Repository>)
        fun showFullscreenProgress()
        fun hideFullscreenProgress()
        fun showError(errorMessage: String)
        fun openPullRequestScreen(repository: Repository, userName: String)
        fun showEmptyState()
        fun hideEmptyState()
        fun showFooterLoader()
        fun hideFooterLoader()
        fun clearList()
    }
}