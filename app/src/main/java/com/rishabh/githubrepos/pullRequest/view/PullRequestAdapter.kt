package com.rishabh.githubrepos.pullRequest.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.rishabh.githubrepos.R
import com.rishabh.githubrepos.models.pullRequest.PullRequest
import java.text.SimpleDateFormat

class PullRequestAdapter(private val pullRequests: List<PullRequest>) : RecyclerView.Adapter<PullRequestAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return LayoutInflater.from(parent.context).inflate(R.layout.pull_request_list_item_layout, parent, false)
                .let { ViewHolder(it) }
    }

    override fun getItemCount(): Int = pullRequests.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(pullRequests[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.footer_tv)
        lateinit var footerTv: TextView
        @BindView(R.id.pull_req_title_tv)
        lateinit var titleTv: TextView

        init {
            ButterKnife.bind(this, itemView)
        }

        fun bind(pullRequest: PullRequest) {
            titleTv.text = pullRequest.title
            val date = pullRequest.createdAt
            val dt = SimpleDateFormat("d MMM yyyy")
            footerTv.text = "#${pullRequest.number} opened on ${dt.format(date)} by ${pullRequest.user.userName}"
        }
    }
}