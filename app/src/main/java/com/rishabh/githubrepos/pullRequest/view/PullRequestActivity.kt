package com.rishabh.githubrepos.pullRequest.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.rishabh.githubrepos.R
import com.rishabh.githubrepos.models.pullRequest.PullRequest
import com.rishabh.githubrepos.pullRequest.presenter.PullRequestPresenter
import com.wang.avi.AVLoadingIndicatorView
import dagger.android.AndroidInjection
import javax.inject.Inject


class PullRequestActivity : MvpActivity<PullRequestPresenter.View, PullRequestPresenter>(), PullRequestPresenter.View {

    companion object {
        private const val USER_NAME = "username"
        private const val REPOSITORY = "repository"
        fun getIntent(context: Context, userName: String, repository: String) =
                Intent(context, PullRequestActivity::class.java).apply {
                    putExtra(USER_NAME, userName)
                    putExtra(REPOSITORY, repository)
                }
    }

    @Inject
    lateinit var pullRequestPresenter: PullRequestPresenter
    @BindView(R.id.user_name_tv)
    lateinit var userNameTv: TextView
    @BindView(R.id.pull_req_rv)
    lateinit var pullReqRv: RecyclerView
    @BindView(R.id.progress_indicator)
    lateinit var progressIndicator: AVLoadingIndicatorView
    @BindView(R.id.empty_state_view)
    lateinit var emptyStateView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pull_request_activity)
        ButterKnife.bind(this)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        presenter.init(intent.extras.getString(USER_NAME)!!,
                intent.extras.getString(REPOSITORY)!!)
    }

    override fun createPresenter(): PullRequestPresenter {
        return pullRequestPresenter
    }

    override fun setHeaderName(headerText: String) {
        userNameTv.text = headerText
    }

    override fun populateRepositories(pullRequests: List<PullRequest>) {
        pullReqRv.adapter = PullRequestAdapter(pullRequests)
    }

    override fun showFullscreenProgress() {
        progressIndicator.visibility = View.VISIBLE
    }

    override fun hideFullscreenProgress() {
        progressIndicator.visibility = View.GONE
    }

    override fun showError(errorMessage: String) {
        Snackbar.make(findViewById(R.id.parent_container), errorMessage, Snackbar.LENGTH_SHORT).show()
    }

    override fun showEmptyState() {
        emptyStateView.visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
        // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
