package com.rishabh.githubrepos.pullRequest.presenter

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.rishabh.githubrepos.models.pullRequest.PullRequest
import com.rishabh.githubrepos.network.GithubService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class PullRequestPresenter(private val githubService: GithubService) : MvpBasePresenter<PullRequestPresenter.View>() {

    fun init(userName: String, repository: String) {
        getPullRequest(userName, repository)
        ifViewAttached { it.setHeaderName("$userName/$repository") }
    }

    private fun getPullRequest(userName: String, repository: String) {
        ifViewAttached { it.showFullscreenProgress() }
        githubService.getUserRepoPullsList(userName, repository)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onGetPullRequestSuccess(it) }, { onGetPullRequestFailed(it) })
    }

    private fun onGetPullRequestFailed(error: Throwable) {
        ifViewAttached {
            it.hideFullscreenProgress()
            it.showError(error.localizedMessage)
        }
    }

    private fun onGetPullRequestSuccess(response: Response<List<PullRequest>>?) {
        ifViewAttached {
            it.hideFullscreenProgress()
            if (response?.body() == null || response.body()?.isEmpty() == true) {
                it.showEmptyState()
            } else {
                it.populateRepositories(response.body()!!)
            }
        }
    }

    interface View : MvpView {
        fun populateRepositories(pullRequest: List<PullRequest>)
        fun showFullscreenProgress()
        fun hideFullscreenProgress()
        fun showError(errorMessage: String)
        fun showEmptyState()
        fun setHeaderName(headerText: String)
    }
}