package com.rishabh.githubrepos.dagger

import com.rishabh.githubrepos.network.GithubService
import com.rishabh.githubrepos.pullRequest.presenter.PullRequestPresenter
import com.rishabh.githubrepos.pullRequest.view.PullRequestActivity
import dagger.Module
import dagger.Provides

@Module
class PullRequestModule {

    @Provides
    internal fun provideReposView(mainActivity: PullRequestActivity): PullRequestPresenter.View {
        return mainActivity
    }

    @Provides
    internal fun provideReposPresenter(apiService: GithubService): PullRequestPresenter {
        return PullRequestPresenter(apiService)
    }
}