package com.rishabh.githubrepos.models.repository

import com.google.gson.annotations.SerializedName

class Repository(
        @SerializedName("id")
        var id: Long? = null,
        @SerializedName("name")
        var name: String? = null,
        @SerializedName("full_name")
        var fullName: String? = null,
        @SerializedName("description")
        var description: String? = null,
        @SerializedName("stargazers_count")
        var stargazersCount: Long? = null,
        @SerializedName("language")
        var language: String? = null,
        @SerializedName("watchers")
        var watchers: Long? = null
)