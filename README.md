# Github Repositorys and Pull Request listing App

This a sample android app built completely on Kotlin. The app has two screens, one is The Repositories screen where a user can type and see all the public repositories of that user. Second screen is a List of pull request screen which shows all the open pull request on a given repository. 

## Libraries used
 - Retrofit
 - Dagger
 - RxJava
 - ButteKnife
 - Mosby (Mvp library)
 
## Screenshots
 
![Alt text](https://i.imgur.com/ZJ2AYcw.png "Repository Screen")

![Alt text](https://i.imgur.com/anYnRMV.png "Pull Request Screen")

