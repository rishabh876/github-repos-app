package com.rishabh.githubrepos.models.pullRequest

import com.google.gson.annotations.SerializedName
import java.util.*

class PullRequest(@SerializedName("title") var title: String? = null,
                  @SerializedName("created_at") var createdAt: Date,
                  @SerializedName("number") var number: Int? = 0,
                  @SerializedName("user") var user: User) {

    class User(@SerializedName("login") var userName: String)
}