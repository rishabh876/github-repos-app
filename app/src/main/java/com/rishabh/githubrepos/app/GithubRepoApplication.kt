package com.rishabh.githubrepos.app

import com.rishabh.githubrepos.dagger.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class GithubRepoApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out GithubRepoApplication> {
        return DaggerAppComponent.builder().create(this)
    }
}