package com.rishabh.githubrepos.network

import com.rishabh.githubrepos.models.pullRequest.PullRequest
import com.rishabh.githubrepos.models.repository.Repository
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {

    @GET(NetworkConstants.ApiEndpoint.USER_REPOS)
    fun getUserReposList(@Path(NetworkConstants.PathParams.USER_NAME) username: String,
                         @Query(NetworkConstants.QueryParam.TYPE) type: String = NetworkConstants.QueryParam.TYPE_OWNER,
                         @Query(NetworkConstants.QueryParam.SORT) sort: String = NetworkConstants.QueryParam.SORT_UPDATED,
                         @Query(NetworkConstants.QueryParam.PAGE) page: Int = 1): Single<Response<List<Repository>>>

    @GET(NetworkConstants.ApiEndpoint.REPO_PULLS)
    fun getUserRepoPullsList(@Path(NetworkConstants.PathParams.OWNER) owner: String,
                             @Path(NetworkConstants.PathParams.REPO) repo: String,
                             @Query(NetworkConstants.QueryParam.STATE) state: String = NetworkConstants.QueryParam.OPEN_STATE): Single<Response<List<PullRequest>>>
}