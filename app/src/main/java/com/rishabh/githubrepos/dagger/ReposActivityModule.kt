package com.rishabh.githubrepos.dagger

import com.rishabh.githubrepos.network.GithubService
import com.rishabh.githubrepos.repos.presenter.ReposPresenter
import com.rishabh.githubrepos.repos.view.ReposActivity
import dagger.Module
import dagger.Provides

@Module
class ReposActivityModule {

    @Provides
    internal fun provideReposView(mainActivity: ReposActivity): ReposPresenter.View {
        return mainActivity
    }

    @Provides
    internal fun provideReposPresenter(apiService: GithubService): ReposPresenter {
        return ReposPresenter(apiService)
    }
}