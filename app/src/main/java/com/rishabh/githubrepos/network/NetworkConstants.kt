package com.rishabh.githubrepos.network

object NetworkConstants {
    const val BASE_URL = "https://api.github.com"

    object ApiEndpoint {
        const val USER_REPOS = "/users/{" + PathParams.USER_NAME + "}/repos"
        const val REPO_PULLS = "/repos/{" + PathParams.OWNER + "}/{" + PathParams.REPO + "}/pulls"
    }

    object QueryParam{
        const val STATE = "state"
        const val OPEN_STATE = "open"
        const val TYPE = "type"
        const val TYPE_OWNER = "owner"
        const val SORT = "sort"
        const val SORT_UPDATED = "updated"
        const val PAGE = "page"
    }

    object PathParams {
        const val USER_NAME = "username"
        const val OWNER = "owner"
        const val REPO = "repo"
    }
}