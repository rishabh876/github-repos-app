package com.rishabh.githubrepos.repos.view

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.rishabh.githubrepos.R
import com.rishabh.githubrepos.models.repository.Repository
import com.rishabh.githubrepos.network.GithubService
import com.rishabh.githubrepos.pullRequest.view.PullRequestActivity
import com.rishabh.githubrepos.repos.presenter.ReposPresenter
import com.wang.avi.AVLoadingIndicatorView
import dagger.android.AndroidInjection
import javax.inject.Inject

class ReposActivity : MvpActivity<ReposPresenter.View,
        ReposPresenter>(),
        ReposPresenter.View,
        ReposAdapter.Listener {

    @Inject
    lateinit var reposPresenter: ReposPresenter
    @BindView(R.id.repos_rv)
    lateinit var reposRv: RecyclerView
    @BindView(R.id.progress_indicator)
    lateinit var progressIndicator: AVLoadingIndicatorView
    @BindView(R.id.search_view)
    lateinit var searchView: SearchView
    @BindView(R.id.empty_state_view)
    lateinit var emptyStateView: View

    private val LAST_QUERY = "last_query"
    private var lastQuery: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.repos_activity)
        ButterKnife.bind(this)
        initView(savedInstanceState)
    }

    private fun initView(savedInstanceState: Bundle?) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                presenter.onUserNameSubmit(query)
                lastQuery = query
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
        reposRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                onScrolled()
            }
        })
        if (savedInstanceState != null && savedInstanceState.containsKey(LAST_QUERY)) {
            searchView.setQuery(savedInstanceState.getString(LAST_QUERY), true)
        }
    }

    override fun createPresenter(): ReposPresenter {
        return reposPresenter
    }

    private fun onScrolled() {
        val lastItem = (reposRv.layoutManager as LinearLayoutManager).itemCount
        val currentlyVisibleItem = (reposRv.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
        if (lastItem - currentlyVisibleItem < 5) {
            getPresenter().getMoreRepos()
        }
    }

    override fun addRepos(repos: List<Repository>) {
        if (reposRv.adapter == null) {
            reposRv.adapter = ReposAdapter(ArrayList(repos), this)
        } else {
            (reposRv.adapter as? ReposAdapter)?.addItems(repos)
        }
        reposRv.visibility = View.VISIBLE
    }

    override fun showFullscreenProgress() {
        progressIndicator.visibility = View.VISIBLE
        reposRv.visibility = View.GONE
    }

    override fun hideFullscreenProgress() {
        progressIndicator.visibility = View.GONE
    }

    override fun showError(errorMessage: String) {
        Snackbar.make(findViewById(R.id.parent_container), errorMessage, Snackbar.LENGTH_SHORT).show()
    }

    override fun showEmptyState() {
        emptyStateView.visibility = View.VISIBLE
        reposRv.visibility = View.GONE
    }

    override fun hideEmptyState() {
        emptyStateView.visibility = View.GONE
    }

    override fun showFooterLoader() {
        (reposRv.adapter as? ReposAdapter)?.showFooter()
    }

    override fun hideFooterLoader() {
        (reposRv.adapter as? ReposAdapter)?.removeFooter()
    }

    override fun clearList() {
        reposRv.adapter = null
    }

    override fun openPullRequestScreen(repository: Repository, userName: String) {
        PullRequestActivity.getIntent(this, userName, repository.name!!).let {
            startActivity(it)
        }
    }

    override fun onRepoClicked(repository: Repository) {
        presenter.onRepositoryClicked(repository)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        lastQuery?.run {
            outState?.putString(LAST_QUERY, lastQuery)
        }
    }
}
