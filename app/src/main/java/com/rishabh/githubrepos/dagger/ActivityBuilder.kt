package com.rishabh.githubrepos.dagger

import com.rishabh.githubrepos.pullRequest.view.PullRequestActivity
import com.rishabh.githubrepos.repos.view.ReposActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [ReposActivityModule::class])
    abstract fun bindRepoActivity(): ReposActivity

    @ContributesAndroidInjector(modules = [PullRequestModule::class])
    abstract fun bindPullReqActivity(): PullRequestActivity

}