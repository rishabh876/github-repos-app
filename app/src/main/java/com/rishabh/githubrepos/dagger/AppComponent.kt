package com.rishabh.githubrepos.dagger

import com.rishabh.githubrepos.app.GithubRepoApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    ActivityBuilder::class])
interface AppComponent : AndroidInjector<GithubRepoApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<GithubRepoApplication>()
}